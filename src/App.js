import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faBroom,
  faCut,
  faDumbbell,
  faHandHoldingHeart,
  faHandPaper,
  faHotTub,
  faIdCard,
  faSearch,
  faShoePrints
} from "@fortawesome/free-solid-svg-icons";
import { Router } from "@reach/router";
import React from "react";
import { createUseStyles } from "react-jss";
import Header from "./components/Header";
import "./css/style.css";
import About from "./pages/About";
import Activities from "./pages/Activities";
import Companies from "./pages/Companies";
import Facilities from "./pages/Facilities";
import Home from "./pages/Home";

const useStyles = createUseStyles({
  mainWrapper: {}
});

library.add(
  faSearch,
  faHandPaper,
  faCut,
  faDumbbell,
  faBroom,
  faHandHoldingHeart,
  faHotTub,
  faShoePrints,
  faIdCard
);

const App = () => {
  const classes = useStyles();
  return (
    <>
      <Header />
      <main className={classes.mainWrapper}>
        <Router>
          <Home path="/" />
          <Activities path="/activities" />
          <Facilities path="/facilities" />
          <About path="/about" />
          <Companies path="/companies" />
        </Router>
      </main>
    </>
  );
};

export default App;
