import {
  GET_ACTIVITYLIST,
  GET_CATEGORYLIST,
  GET_COMPANYLIST,
  SET_CATEGORYFILTER,
  SET_LOADER
} from "./Action-types";

function getActivityList(activityList) {
  return {
    type: GET_ACTIVITYLIST,
    payload: {
      activityList
    }
  };
}

function getCategoryList(categoryList) {
  return {
    type: GET_CATEGORYLIST,
    payload: {
      categoryList
    }
  };
}

function getCompanyList(companyList) {
  return {
    type: GET_COMPANYLIST,
    payload: {
      companyList
    }
  };
}

function setLoading() {
  return {
    type: SET_LOADER
  };
}
export function setCategoryFilter(category) {
  return {
    type: SET_CATEGORYFILTER,
    payload: {
      category
    }
  };
}

export function fetchActivityList() {
  return dispatch => {
    dispatch(setLoading());
    fetch("https://babooka-api.now.sh/activities")
      .then(response => {
        return response.json();
      })
      .then(data => {
        console.log(data);
        dispatch(getActivityList(data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function fetchCategoryList() {
  return dispatch => {
    dispatch(setLoading());
    fetch("https://babooka-api.now.sh/categories")
      .then(response => {
        return response.json();
      })
      .then(data => {
        dispatch(getCategoryList(data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function fetchCompanyList() {
  return dispatch => {
    fetch("https://babooka-api.now.sh/companies")
      .then(response => {
        return response.json();
      })
      .then(data => {
        dispatch(getCompanyList(data));
      })
      .catch(error => {
        console.log(error);
      });
  };
}
