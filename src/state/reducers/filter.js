import { SET_CATEGORYFILTER } from "../actions";

const initialState = { category: null };

export default function filter(state = initialState, action) {
  switch (action.type) {
    case SET_CATEGORYFILTER:
      return {
        ...state,
        category: action.payload.category
      };
    default:
      return state;
  }
}
