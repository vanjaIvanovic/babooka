import { GET_ACTIVITYLIST, SET_CATEGORYFILTER, SET_LOADER } from "../actions";

const initialState = {
  activityList: [],
  filteredActivities: [],
  loading: false
};

export default function activityList(state = initialState, action) {
  switch (action.type) {
    case SET_LOADER:
      return {
        ...state,
        loading: true
      };

    case GET_ACTIVITYLIST:
      return {
        ...state,
        loading: false,
        activityList: action.payload.activityList,
        filteredActivities: action.payload.activityList
      };
    case SET_CATEGORYFILTER:
      return {
        ...state,
        loading: false,
        filteredActivities: state.activityList.filter(activity => {
          return activity.categoryIds.includes(action.payload.category.id);
        })
      };

    default:
      return state;
  }
}
