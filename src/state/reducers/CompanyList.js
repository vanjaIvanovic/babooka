import { GET_COMPANYLIST } from "../actions";

const initialState = {
  companyList: []
};

export default function companyList(state = initialState, action) {
  switch (action.type) {
    case GET_COMPANYLIST:
      return {
        ...state,
        companyList: action.payload.companyList
      };
    default:
      return state;
  }
}
