import { GET_CATEGORYLIST, SET_LOADER } from "../actions";

const initialState = {
  categoryList: [],
  loading: false
};

export default function categoryList(state = initialState, action) {
  switch (action.type) {
    case SET_LOADER:
      return {
        ...state,
        loading: true
      };
    case GET_CATEGORYLIST:
      return {
        ...state,
        loading: false,
        categoryList: action.payload.categoryList
      };
    default:
      return state;
  }
}
