import { combineReducers } from "redux";
import activityList from "./ActivityList";
import categoryList from "./CategoryList";
import companyList from "./CompanyList";
import filter from "./filter";

export default combineReducers({
  activityList,
  categoryList,
  companyList,
  filter
});
