const theme = {
  colors: {
    white: "#ffffff",
    yellow: "#ffbf00",
    blue: "#4a90e2",
    lightGrey: "#D3D3D3",
    grey: "#A0A0A0",
    black: "#000000"
  },
  margin: {
    s: "10px",
    m: "15px",
    l: "20px",
    xl: "30px",
    xxl: "50px",
    center: "0 auto"
  },
  padding: {
    xs: "10px",
    s: "30px",
    m: "50px",
    l: "60px"
  },
  textSizes: {
    xs: "12px",
    s: "14px",
    m: "16px",
    l: "20px",
    xl: "25px",
    xxl: "35px"
  },
  borderRadius: {
    s: "15px",
    m: "20px"
  },
  boxShadows: {
    light: "0 2px 5px 0 rgba(0, 0, 0, 0.08)",
    mediumLight: "0 2px 10px 0 rgba(0, 0, 0, 0.08)",
    medium: "0 2px 13px 0 rgba(0, 0, 0, 0.1)",
    dark: "2px 2px 10px 0 rgba(0, 0, 0, 0.08)",
    darker: "2px 5px 15px 0 rgba(0, 0, 0, 0.1)"
  },
  width: {
    s: "100px",
    full: "100%"
  },
  height: {
    s: "100px"
  }
};

export default theme;
