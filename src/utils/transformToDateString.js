export default function transformToDateString(date) {
  const dateString = new Date(date).toLocaleDateString("sv-SE", {
    weekday: "short",
    year: "numeric",
    month: "long",
    day: "numeric"
  });

  return dateString.charAt(0).toUpperCase() + dateString.slice(1);
}
