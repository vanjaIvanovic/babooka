export default function handleInputValues(event, setInputValues, inputValues) {
  const { name, value } = event.target;
  setInputValues({ ...inputValues, [name]: value });
}
