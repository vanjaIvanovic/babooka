import React from "react";
import ActivityCards from "../../components/ActivityCards";
import Categories from "../../components/Categories";
import Hero from "../../components/Hero";

const Home = () => {
  return (
    <>
      <Hero isHome={true} title="Boka din aktivitet" />
      <Categories />
      <ActivityCards />
    </>
  );
};

export default Home;
