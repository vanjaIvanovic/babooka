import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles(theme => ({
  form: {
    display: "flex",
    alignItems: "center",
    position: "relative"
  },
  input: {
    flexGrow: 1,
    padding: theme.padding.xs,
    paddingLeft: theme.padding.m,
    boxSizing: "border-box",
    borderRadius: theme.borderRadius.s,
    borderWidth: 0,
    boxShadow: theme.boxShadows.light,
    backgroundColor: theme.colors.white,
    fontSize: theme.textSizes.l
  },
  searchIcon: {
    position: "absolute",
    left: theme.margin.l,
    color: theme.colors.yellow
  },
  submitButton: {
    padding: `13px ${theme.padding.s}`,
    marginLeft: theme.margin.l,
    borderRadius: theme.borderRadius.s,
    boxShadow: theme.boxShadows.medium,
    borderWidth: 0,
    backgroundColor: theme.colors.white,
    color: theme.colors.yellow,
    fontSize: theme.textSizes.m,
    fontWeight: "800"
  }
}));

const SearchInput = () => {
  const classes = useStyles();
  return (
    <form className={classes.form}>
      <input
        className={classes.input}
        type="search"
        id="site-search"
        name="search"
        placeholder="Sök aktivitet eller företag..."
        aria-label="Sök aktivitet eller företag"
      />
      <FontAwesomeIcon icon="search" size="lg" className={classes.searchIcon} />

      <button type="submit" className={classes.submitButton}>
        Sök
      </button>
    </form>
  );
};

export default SearchInput;
