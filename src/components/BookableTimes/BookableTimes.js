import React from "react";
import { createUseStyles } from "react-jss";
import handleInputValues from "../../utils/handleInputValues";

const useStyles = createUseStyles(theme => ({
  bookableTimeList: {
    listStyle: "none"
  },
  cardInfo: {
    padding: "20px 25px",
    marginBottom: theme.margin.m,
    marginRight: 20,
    borderRadius: theme.borderRadius.s,
    backgroundColor: theme.colors.yellow,
    alignSelf: "center",
    listStyle: "none",
    textAlign: "center",
    color: theme.colors.white
  },

  radioButton: {
    opacity: 0,
    position: "absolute"
  }
}));

const BookableTimes = ({ bookableTime, setInputValues, inputValues }) => {
  const classes = useStyles();

  return (
    <>
      <li className={classes.bookableTimeList}>
        <input
          type="radio"
          id={bookableTime.id}
          value={`${bookableTime.date} ${bookableTime.startTime} ${bookableTime.endTime}`}
          name="bookableTime"
          className={classes.radioButton}
          onChange={event =>
            handleInputValues(event, setInputValues, inputValues)
          }
        />
        <label
          tabindex="0"
          htmlFor={bookableTime.id}
          className={classes.cardInfo}
        >
          {bookableTime.startTime} - {bookableTime.endTime}
        </label>
      </li>
    </>
  );
};

export default BookableTimes;
