import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { createUseStyles } from "react-jss";
import { useDispatch } from "react-redux";
import { setCategoryFilter } from "../../state/actions";

const useStyles = createUseStyles(theme => ({
  cardCategory: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: theme.width.s,
    marginRight: theme.margin.l,
    color: theme.colors.grey,
    fontSize: theme.textSizes.s,
    marginLeft: theme.margin.s
  },
  card: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: theme.height.s,
    width: theme.width.s,
    marginBottom: theme.margin.m,
    backgroundColor: theme.colors.white,
    borderRadius: theme.borderRadius.m,
    borderWidth: 0,
    boxShadow: theme.boxShadows.dark
  },
  searchIcon: props => ({
    color: props.color
  })
}));

const Card = ({ category }) => {
  const classes = useStyles(category);
  const dispatch = useDispatch();

  const filterCategories = () => {
    dispatch(setCategoryFilter(category));
  };

  return (
    <div className={classes.cardCategory}>
      <button type="button" className={classes.card} onClick={filterCategories}>
        <FontAwesomeIcon
          icon={category.icon}
          size="2x"
          className={classes.searchIcon}
        />
      </button>
      {category.name}
    </div>
  );
};

export default Card;
