import { Link } from "@reach/router";
import React from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles(theme => ({
  navigationItem: {
    margin: "0 24px",
    display: "inline-block"
  },
  link: {
    textDecoration: "none",
    fontSize: "1.1rem",
    color: theme.colors.black
  }
}));

const NavItem = ({ data }) => {
  const classes = useStyles();
  return (
    <li className={classes.navigationItem}>
      <Link className={classes.link} to={`/${data.path}`}>
        {data.link}
      </Link>
    </li>
  );
};

export default NavItem;
