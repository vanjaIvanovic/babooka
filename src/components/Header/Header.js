import { Link } from "@reach/router";
import React from "react";
import { createUseStyles } from "react-jss";
import Nav from "../Nav";

const useStyles = createUseStyles(theme => ({
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    minHeight: 70,
    padding: "0 9rem 0 5rem"
  },
  link: {
    color: theme.colors.yellow,
    fontSize: theme.textSizes.l,
    textDecoration: "none"
  }
}));

const Header = () => {
  const classes = useStyles();
  return (
    <header className={classes.header}>
      <h1>
        <Link className={classes.link} to="/">
          Babooka
        </Link>
      </h1>
      <Nav />
    </header>
  );
};

export default Header;
