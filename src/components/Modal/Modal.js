import React, { useState } from "react";
import { createUseStyles } from "react-jss";
import CloseButton from "../CloseButton";
import Form from "../Form";
import ChoiceOfTime from "../ChoiceOfTime";
import SelectOfDate from "../SelectOfDate";
import ActivityInfo from "../ActivityInfo";

const useStyles = createUseStyles(theme => ({
  bookingConfirmMsg: {
    textAlign: "center",
    fontSize: theme.textSizes.xl,
    color: theme.colors.blue
  }
}));

const Modal = ({ closeModal, activity, company }) => {
  const classes = useStyles();
  const [bookableDates, setBookableDates] = useState({});
  const [chosenDate, setChosenDate] = useState(activity.bookableTimes[0].date);
  const [booked, setBooked] = useState(false);

  const [inputValues, setInputValues] = useState({
    name: "",
    phone: "",
    email: "",
    comment: "",
    bookableTime: ""
  });

  function handleSubmit(event) {
    event.preventDefault();
    localStorage.setItem("booking", JSON.stringify(inputValues));
    setBooked(true);
    resetFormFields();
  }

  function resetFormFields() {
    setInputValues({
      name: "",
      phone: "",
      email: "",
      comment: "",
      bookableTime: ""
    });
  }

  return (
    <article>
      <CloseButton closeModal={closeModal} />
      <ActivityInfo activity={activity} company={company} />
      <form onSubmit={handleSubmit}>
        <SelectOfDate
          activity={activity}
          bookableDates={bookableDates}
          setBookableDates={setBookableDates}
          setChosenDate={setChosenDate}
        />
        <ChoiceOfTime
          chosenDate={chosenDate}
          bookableDates={bookableDates}
          inputValues={inputValues}
          setInputValues={setInputValues}
        />
        {booked && (
          <div className={classes.bookingConfirmMsg}>Tack för din bokning!</div>
        )}
        <Form inputValues={inputValues} setInputValues={setInputValues} />
      </form>
    </article>
  );
};

export default Modal;
