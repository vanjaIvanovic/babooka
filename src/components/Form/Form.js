import React from "react";
import { createUseStyles } from "react-jss";
import handleInputValues from "../../utils/handleInputValues";

const useStyles = createUseStyles(theme => ({
  formContainer: {
    backgroundColor: theme.colors.yellow,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20
  },
  formElements: {
    display: "flex",
    justifyContent: "space-between",
    padding: theme.padding.m,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20
  },
  inputContainer: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    marginRight: theme.margin.xxl,
    padding: "2px 0"
  },
  inputField: {
    borderRadius: theme.borderRadius.s,
    borderWidth: 0,
    marginBottom: theme.margin.xl,
    padding: `15px ${theme.padding.s}`,
    boxShadow: theme.boxShadows.darker,
    "&:last-child": {
      marginBottom: 0
    }
  },
  textArea: {
    width: "40%",
    borderRadius: theme.borderRadius.s,
    boxShadow: theme.boxShadows.light,
    padding: "15px 20px"
  },
  buttonContainer: {
    display: "flex",
    width: theme.width.full,
    justifyContent: "flex-end",
    backgroundColor: theme.colors.white,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20
  },
  bookButton: {
    padding: "15px 70px",
    borderRadius: theme.borderRadius.s,
    backgroundColor: theme.colors.blue,
    color: theme.colors.white,
    borderWidth: 0,
    margin: "30px 55px 30px 0",
    fontWeight: 800,
    fontSize: theme.textSizes.m
  }
}));

const Form = ({ setInputValues, inputValues }) => {
  const classes = useStyles();

  return (
    <section className={classes.formContainer}>
      <div className={classes.formElements}>
        <div className={classes.inputContainer}>
          <label htmlFor="name" className="srOnly">
            Skriv ditt förnamn och efternamn
          </label>
          <input
            className={classes.inputField}
            value={inputValues.name}
            onChange={event =>
              handleInputValues(event, setInputValues, inputValues)
            }
            type="text"
            id="name"
            name="name"
            placeholder="Förnamn Efternamn"
            required
          />

          <label htmlFor="tel" className="srOnly">
            Skriv ditt telefonnummer
          </label>
          <input
            className={classes.inputField}
            value={inputValues.phone}
            onChange={event =>
              handleInputValues(event, setInputValues, inputValues)
            }
            type="tel"
            id="tel"
            name="phone"
            placeholder="Telefon"
            required
          />

          <label htmlFor="email" className="srOnly">
            Skriv din mailadress
          </label>
          <input
            className={classes.inputField}
            value={inputValues.email}
            onChange={event =>
              handleInputValues(event, setInputValues, inputValues)
            }
            type="email"
            id="email"
            name="email"
            placeholder="Epostadress"
            required
          />
        </div>
        <label htmlFor="comment" className="srOnly">
          Kommentar
        </label>
        <textarea
          className={classes.textArea}
          value={inputValues.comment}
          onChange={event =>
            handleInputValues(event, setInputValues, inputValues)
          }
          id="comment"
          name="comment"
          placeholder="Kommentar"
        />
      </div>
      <div className={classes.buttonContainer}>
        <button className={classes.bookButton} type="submit">
          Boka
        </button>
      </div>
    </section>
  );
};

export default Form;
