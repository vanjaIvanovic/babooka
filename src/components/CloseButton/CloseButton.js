import React from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles(theme => ({
  closeModalWrapper: {
    display: "flex",
    justifyContent: "flex-end",
    padding: 20
  },
  closeModalButton: {
    backgroundColor: "transparent",
    border: "none",
    fontWeight: 800,
    fontSize: theme.textSizes.xl
  }
}));

const CloseButton = ({ closeModal }) => {
  const classes = useStyles();

  return (
    <div className={classes.closeModalWrapper}>
      <button
        type="button"
        className={classes.closeModalButton}
        onClick={() => closeModal(false)}
      >
        X
      </button>
    </div>
  );
};

export default CloseButton;
