import React from "react";
import { createUseStyles } from "react-jss";
import NavItem from "../NavItem";

const useStyles = createUseStyles(theme => ({
  navigation: {
    display: "flex",
    backgroundColor: theme.colors.white,
    justifyContent: "flex-end",
    listStyle: "none",
    alignItems: "center"
  },
  navigationItem: {
    margin: "32px 24px 0 24px",
    display: "inline-block",
    padding: 0
  },
  link: {
    textDecoration: "none",
    fontSize: theme.textSizes.m,
    color: theme.colors.black
  },
  navigationList: {
    margin: 0
  }
}));

const Nav = () => {
  const classes = useStyles();

  const linkArray = [
    {
      id: 1,
      link: "Aktiviteter",
      path: "activities"
    },
    {
      id: 2,
      link: "Anläggningar",
      path: "facilities"
    },
    {
      id: 3,
      link: "Om oss",
      path: "about"
    },
    {
      id: 4,
      link: "För aktörer",
      path: "companies"
    }
  ];

  return (
    <nav className={classes.navigation}>
      <ul className={classes.navigationList}>
        {linkArray.map(link => (
          <NavItem key={link.id} data={link} />
        ))}
      </ul>
    </nav>
  );
};

export default Nav;
