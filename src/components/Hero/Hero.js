import React from "react";
import { createUseStyles } from "react-jss";
import SearchInput from "../SearchInput";

const useStyles = createUseStyles(theme => ({
  section: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: 400,
    backgroundColor: theme.colors.yellow,
    boxShadow: theme.boxShadows.medium
  },
  wrapper: {
    width: theme.width.full,
    maxWidth: 750
  },
  title: {
    fontSize: theme.textSizes.xxl,
    textAlign: "center"
  }
}));

const Hero = ({ isHome, title }) => {
  const classes = useStyles();
  return (
    <section className={classes.section}>
      <div className={classes.wrapper}>
        <h1 className={classes.title}>{title}</h1>
        {isHome ? <SearchInput /> : null}
      </div>
    </section>
  );
};

export default Hero;
