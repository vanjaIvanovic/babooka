import React from "react";
import {
  useTrapFocus,
  useBodyScrollLock,
  ModalPortal
} from "@weahead/react-customizable-modal";

import { createUseStyles } from "react-jss";

const useStyles = createUseStyles(theme => ({
  overlayContainer: {
    width: theme.width.full,
    maxWidth: 1200,
    margin: theme.margin.center,
    borderRadius: theme.borderRadius.m,
    boxShadow: theme.boxShadows.darker,
    backgroundColor: theme.colors.white
  },
  overlayWrapper: {
    position: "fixed",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    padding: 50,
    backgroundColor: "rgba(89, 89, 88, 0.53)",
    overflowY: "scroll"
  }
}));

const CustomModal = ({ children }) => {
  const classes = useStyles();
  useBodyScrollLock();
  const modalRef = useTrapFocus();
  return (
    <ModalPortal id={`customModal`}>
      <div className={classes.overlayWrapper}>
        <div ref={modalRef} className={classes.overlayContainer}>
          {children}
        </div>
      </div>
    </ModalPortal>
  );
};

export default CustomModal;
