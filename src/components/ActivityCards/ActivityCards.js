import React, { useEffect, useState } from "react";
import { createUseStyles } from "react-jss";
import { useDispatch, useSelector } from "react-redux";
import { fetchActivityList, fetchCompanyList } from "../../state/actions";
import ActivityCard from "../ActivityCard";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";

const useStyles = createUseStyles((theme) => ({
  container: {
    textAlign: "center",
  },
  articleWrapper: {
    display: "flex",
    justifyContent: "center",
  },
  totalAmountOfActivities: {
    display: "flex",
    justifyContent: "flex-end",
  },
  cardList: {
    display: "flex",
    flexWrap: "wrap",
    maxWidth: 1015,
    borderTop: `1px solid ${theme.colors.lightGrey}`,
    padding: 0,
  },
  activityList: {
    listStyle: "none",
  },
  loader: {
    padding: 150,
    margin: "0 auto",
  },
  showMoreButton: {
    padding: "15px 70px",
    borderRadius: theme.borderRadius.s,
    backgroundColor: theme.colors.blue,
    color: theme.colors.white,
    borderWidth: 0,
    margin: "30px 55px 30px 0",
    fontWeight: 800,
    fontSize: theme.textSizes.m,
  },
}));

const ActivityCards = () => {
  const classes = useStyles();
  const loading = useSelector((state) => state.activityList.loading);
  const activities = useSelector(
    (state) => state.activityList.filteredActivities
  );

  const companies = useSelector((state) => state.companyList.companyList);

  const dispatch = useDispatch();
  const [numberOfItems, setNumberOfItems] = useState(9);

  useEffect(() => {
    if (!activities.length) {
      dispatch(fetchActivityList());
    }
  }, [activities, dispatch]);

  useEffect(() => {
    if (!companies.length && !loading) {
      dispatch(fetchCompanyList());
    }
  }, [companies, loading, dispatch]);

  function showMoreActivitiesItems() {
    setNumberOfItems(numberOfItems + 9);
  }

  return (
    <div className={classes.container}>
      <div className={classes.articleWrapper}>
        {loading ? (
          <div className={classes.loader}>
            <Loader
              type="Puff"
              color="#00BFFF"
              height={50}
              width={50}
              timeout={3000}
            />
          </div>
        ) : (
          <div>
            <div className={classes.totalAmountOfActivities}>
              {activities.length} resultat
            </div>
            <ul className={classes.cardList}>
              {activities.slice(0, numberOfItems).map((activity) => (
                <li key={activity.id} className={classes.activityList}>
                  <ActivityCard activity={activity} />
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
      {numberOfItems <= activities.length && (
        <button
          className={classes.showMoreButton}
          onClick={showMoreActivitiesItems}
        >
          Visa mer
        </button>
      )}
    </div>
  );
};

export default ActivityCards;
