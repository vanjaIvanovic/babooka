import React, { useEffect } from "react";
import { createUseStyles } from "react-jss";
import transformToDateString from "../../utils/transformToDateString";

const useStyles = createUseStyles(theme => ({
  avaibleTimeText: {
    fontSize: theme.textSizes.l,
    fontWeight: 800,
    textAlign: "center"
  },
  selectionContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    maxWidth: 800,
    margin: theme.margin.center
  },
  selectInput: {
    boxSizing: "border-box",
    padding: "15px 60px 10px 20px",
    fontSize: theme.textSizes.m,
    fontWeight: 500,
    borderRadius: theme.borderRadius.s,
    boxShadow: theme.boxShadows.light,
    borderWidth: 0,
    marginRight: theme.margin.l,
    "&:last-child": {
      marginRight: 0
    }
  },
  dateInput: {
    flexGrow: 1
  }
}));

const SelectOfDate = ({
  activity,
  bookableDates,
  setBookableDates,
  setChosenDate
}) => {
  const classes = useStyles();

  function avaibleTimes() {
    const bookableDates = activity.bookableTimes;
    const availableDates = {};

    bookableDates.forEach(bookableDate => {
      availableDates[bookableDate.date] = availableDates[bookableDate.date]
        ? [...availableDates[bookableDate.date], bookableDate]
        : [bookableDate];
    });

    return availableDates;
  }

  useEffect(() => {
    if (!Object.keys(bookableDates).length) {
      setBookableDates(avaibleTimes());
    }
  }, [bookableDates, avaibleTimes]);

  function getSelectedValue(event) {
    setChosenDate(event.target.value);
  }

  return (
    <>
      <h3 className={classes.avaibleTimeText}>Lediga tider</h3>
      <div className={classes.selectionContainer}>
        <label htmlFor="date" className="srOnly">
          Välj datum
        </label>
        <select
          className={`${classes.selectInput} ${classes.dateInput}`}
          id="date"
          name="dateList"
          form="dateform"
          onChange={getSelectedValue}
        >
          {Object.keys(avaibleTimes()).map(availableDate => {
            return (
              <option key={availableDate} value={availableDate}>
                {transformToDateString(availableDate)}
              </option>
            );
          })}
        </select>
        <label htmlFor="time" className="srOnly">
          Välj tid
        </label>
        <select
          className={classes.selectInput}
          id="time"
          name="timeList"
          form="timeform"
        >
          <option value="1 hour">1 timme</option>
          <option value="2 hours">2 timmar</option>
          <option value="3 hours">3 timmar</option>
          <option value="4 hours">4 timmar</option>
        </select>

        <label htmlFor="person" className="srOnly">
          Välj antal personer
        </label>
        <select
          className={classes.selectInput}
          id="person"
          name="personList"
          form="personform"
        >
          <option value="1 person">1 person</option>
          <option value="2 persons">2 personer</option>
          <option value="3 persons">3 personer</option>
          <option value="4 persons">4 personer</option>
        </select>
      </div>
    </>
  );
};

export default SelectOfDate;
