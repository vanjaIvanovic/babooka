import React from "react";
import { createUseStyles } from "react-jss";
import BookableTimes from "../BookableTimes";
import transformToDateString from "../../utils/transformToDateString";

const useStyles = createUseStyles(theme => ({
  fieldset: {
    border: 0,
    paddingBottom: 20
  },
  firstAvaibleTime: {
    textAlign: "center",
    fontSize: theme.textSizes.m,
    fontWeight: 800,
    paddingTop: theme.padding.s
  },
  cardContainer: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    maxWidth: 800,
    borderTop: "1px solid #DCDCDC",
    paddingTop: 45,
    padding: 0,
    margin: theme.margin.center,
    marginBottom: theme.margin.xxl
  }
}));

const ChoiceOfTime = ({
  chosenDate,
  bookableDates,
  inputValues,
  setInputValues
}) => {
  const classes = useStyles();
  return (
    <fieldset className={classes.fieldset}>
      <legend className={classes.firstAvaibleTime}>
        {transformToDateString(chosenDate)}
      </legend>
      <ul className={classes.cardContainer}>
        {bookableDates[chosenDate] &&
          bookableDates[chosenDate].map(availableTime => (
            <BookableTimes
              key={availableTime.id}
              bookableTime={availableTime}
              inputValues={inputValues}
              setInputValues={setInputValues}
            />
          ))}
      </ul>
    </fieldset>
  );
};

export default ChoiceOfTime;
