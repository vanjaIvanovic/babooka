import React from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles(theme => ({
  sectionWrapper: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0 50px",
    padding: "0 0 50px 0",
    borderBottom: "1px solid grey",
    overflow: "hidden"
  },
  activityName: {
    fontWeight: 800,
    fontSize: theme.textSizes.xl
  },
  infoSection: {
    fontSize: theme.textSizes.m,
    paddingLeft: 35,
    marginRight: 40,
    width: "100%"
  },
  facility: {
    fontWeight: 200,
    fontSize: theme.textSizes.m
  },
  text: {
    fontSize: theme.textSizes.m,
    lineHeight: 1.8,
    paddingBottom: 20
  },
  imgContainer: {
    width: "100%",
    minWidth: 200
  },
  img: {
    borderRadius: theme.borderRadius.m,
    boxShadow: theme.boxShadows.medium,
    objectFit: "cover",
    width: "100%",
    height: "auto"
  },
  facilityInfo: {
    display: "flex",
    justifyContent: "space-between",
    padding: "5px 0"
  },

  facilityContent: {
    display: "flex",
    flexDirection: "column",
    lineHeight: 1.8,
    marginRight: 40,
    "&:last-child": {
      marginRight: 0
    }
  }
}));

const ActivityInfo = ({ activity, company }) => {
  const classes = useStyles();

  return (
    <header className={classes.sectionWrapper}>
      <div className={classes.infoSection}>
        <h1 className={classes.activityName}>{activity.name}</h1>
        <h2 className={classes.facility}>
          {company.name}, {company.city}
        </h2>
        <p className={classes.text}>{activity.description}</p>
        <div className={classes.facilityInfo}>
          <div className={classes.facilityContent}>
            <span>{company.contact.phone}</span>
            <span>{company.contact.email}</span>
          </div>
          <div className={classes.facilityContent}>
            <span>{company.contact.website}</span>
            <span>{company.address}</span>
            <span>{company.postcode}</span>
          </div>
        </div>
      </div>
      <div className={classes.imgContainer}>
        <img
          src={activity.image.src}
          alt={activity.image.description}
          className={classes.img}
        />
      </div>
    </header>
  );
};

export default ActivityInfo;
