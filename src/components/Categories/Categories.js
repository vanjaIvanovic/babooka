import React, { useEffect } from "react";
import { createUseStyles } from "react-jss";
import { useDispatch, useSelector } from "react-redux";
import { fetchCategoryList } from "../../state/actions";
import Card from "../Card";

const useStyles = createUseStyles((theme) => ({
  cardList: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center",
    width: theme.width.full,
    marginTop: -50,
    marginBottom: 130,
  },
}));

const Categories = () => {
  const classes = useStyles();
  const categories = useSelector((state) => state.categoryList.categoryList);
  const loading = useSelector((state) => state.categoryList.loading);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!categories.length && !loading) {
      dispatch(fetchCategoryList());
    }
  }, [categories, loading, dispatch]);

  return (
    <section className={classes.cardList}>
      {categories.map((category) => (
        <Card key={category.id} category={category} />
      ))}
    </section>
  );
};

export default Categories;
