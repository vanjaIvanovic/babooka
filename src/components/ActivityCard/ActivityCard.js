import React, { useState } from "react";
import { createUseStyles } from "react-jss";
import { useSelector } from "react-redux";
import CustomModal from "../CustomModal";
import Modal from "../Modal";

const useStyles = createUseStyles(theme => ({
  card: {
    position: "relative",
    maxWidth: 230,
    marginTop: theme.margin.xl,
    backgroundColor: theme.colors.white,
    borderRadius: theme.borderRadius.m,
    boxShadow: theme.boxShadows.mediumLight,
    borderWidth: 0,
    marginRight: theme.margin.l,
    overflow: "hidden"
  },
  activityImg: {
    width: theme.width.full,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    objectFit: "cover",
    height: 180
  },
  topRight: categoryColor => ({
    position: "absolute",
    top: 8,
    right: 16,
    fontSize: theme.textSizes.xs,
    fontWeight: "bold",
    color: theme.colors.white,
    padding: "4px 15px",
    backgroundColor: categoryColor,
    borderRadius: 7
  }),
  content: {
    padding: "20px 20px 10px 20px",
    display: "flex",
    flexDirection: "column",
    textAlign: "left",
    fontSize: theme.textSizes.s,
    fontWeight: 200
  },
  title: {
    paddingBottom: 5,
    fontWeight: 800,
    fontSize: theme.textSizes.s
  },
  avaibleTimeText: {
    fontSize: theme.textSizes.xs,
    paddingTop: theme.padding.xs,
    paddingBottom: theme.padding.xs
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    borderTop: "1px solid #ececec;",
    fontSize: theme.textSizes.xs,
    paddingTop: 3
  },
  button: {
    backgroundColor: theme.colors.yellow,
    borderRadius: 10,
    borderWidth: 0,
    padding: "8px 20px",
    fontSize: theme.textSizes.xs,
    color: theme.colors.white,
    fontWeight: "bold"
  }
}));

const ActivityCard = ({ activity }) => {
  const [showModal, setShowModal] = useState(false);
  const company = useSelector(state =>
    state.companyList.companyList.find(
      company => company.id === activity.companyId
    )
  );
  const categories = useSelector(state => state.categoryList.categoryList);
  const category = categories.find(
    category => category.id === activity.categoryIds[0]
  );

  const classes = useStyles(category.color);

  return (
    <>
      <article className={classes.card}>
        <img
          src={activity.image.src}
          alt={activity.image.description}
          className={classes.activityImg}
        />
        <p className={classes.topRight}>{category.name}</p>
        <div className={classes.content}>
          <h1 className={classes.title}>{activity.name}</h1>
          <span>{company.name},</span>
          <span>{company.city}</span>
          <span className={classes.avaibleTimeText}>
            Första lediga tiden: {activity.bookableTimes[0].date}
          </span>
          <div className={classes.container}>
            <p>fr. {activity.price} kr</p>
            <button
              type="button"
              className={classes.button}
              onClick={() => setShowModal(true)}
            >
              Boka
            </button>
          </div>
        </div>
      </article>
      {showModal && (
        <CustomModal isOpen={showModal}>
          <Modal
            closeModal={setShowModal}
            activity={activity}
            company={company}
          />
        </CustomModal>
      )}
    </>
  );
};

export default ActivityCard;
