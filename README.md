# Babooka

## Introduktion

Babooka är en sida där man kan boka många olika tjänster eller aktiviteter. På huvudsidan finns en lista med de olika tjänster/aktiviteter som finns tillgängliga och är bokbara. Användaren ska kunna filtrera genom att söka eller på kategori.

## Projektet

Designen hittar ni på [Zeplin](https://zpl.io/agdelN0). Projektet går ut på att man förvandlar designen till en fungerande app.

### Krav

- Applikationen ska efterlikna designen på Zeplin.
- Länkerna i menyn ska navigeras till olika sidor. Dessa sidor behöver dock inte designas.
  - Babooka "loggan" ska länkas till startsida.
- Datat ska kolla från en API. En mock-api finns på plats.
  - url: https://babooka-api.now.sh
  - endpoints:
    - /activities/[:id]
    - /categories/[:id]
    - /companies/[:id]
  - Datat sparas ner i din globala state hantering (Redux). För att kunna hämta data behöver man använda en såkallad middleware. Enklast är att använda [Redux Thunk](https://github.com/reduxjs/redux-thunk).
- Man ska kunna filtrera listan med aktiviteter genom att söka efter aktiviteter eller företag.
  - Får man ingen resultat, så ska det meddelas till användaren.
- Man ska kunna filtrera listan med aktiviteter genom att klicka på kategorierna.
  - Både filter ska kunna funka tillsammans.
  - Söker man på "thaimassage" och klickar på kategori "Massage" så ska endast de resultat visas som matchar både filtreringarna.
- Varja kategori har sin egen färg och ikon. Sök upp lämplig ikon från t.ex. [Font Awesome](https://fontawesome.com/).
- Max 12 aktiviteter ska visas på sidan, finns det fler så ska man kunna klicka på "Visa fler" knappen för att hämta flera aktiviteter.
  - Dessa hamnar då löpande under resten av aktiviteterna.
  - Finns det inte fler aktiviteter att hämta så ska knappen vara `disabled`.
- Klickar man på "boka" på en aktivitet så ska en modal öppnas.
  - I modalen ska man kunna välja en ledig tid och boka den genom att fylla i sina uppgifter.
  - Glöm inte valideringen!
  - Användaren ska få en bekräftelse att bokningen har gått igenom.
  - Bokningen ska sparas ner i `sessionStorage`/`localStorage`/`cookies` eller dyl.
- Applikationen ska vara logiskt uppbyggt med hjälp av komponenter

#### Stack

Tekniker/Best practices som ska användas är följande:

- [Semantik](https://developer.mozilla.org/en-US/docs/Glossary/Semantics)
- [Tillgänglighet](https://www.w3.org/WAI/fundamentals/accessibility-intro/)
- [CSS Grid](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout)
- [Flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout)
- [Create React App](https://create-react-app.dev/)
- [React-JSS](https://cssinjs.org/react-jss?v=v10.0.4)
- [React Redux](https://react-redux.js.org/)
- [React Hooks](https://reactjs.org/docs/hooks-intro.html)
- [Reach Router](https://reach.tech/router)

#### Git

Alla ska använda git för att versionshantera sitt projekt. Använd gärna [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/). Det finns git extensions för det här: [Git Flow AVH](https://github.com/petervanderdoes/gitflow-avh).

Bjud in handledaren till era Git repo och se till att skapa en merge request (MR) för varje feature. Handledaren kommer gå igenom dessa och lämna feedback. Vid behov kommer handledaren tillsammans med dig kolla igenom din MR.

#### Demo

Tanken är att vi kan köra applikationen live på webben. För att göra detta kan man använda sig utav [Now](https://now.sh/). Det är en tjänst där man enkelt kan deploy:a sin applikation utan konfiguration.

### Tips

Innan du börjar, försök att tänka igenom hur du ska börja och hur du ska dela upp projeketet. Skriv ner, steg för steg, vad det är du behöver göra för att uppfylla de krav som finns.

- Använd en tydlig mappstruktur.
- Namnge dina komponenter/funktioner/variabler på ett sätt så alla vet vad som händer och skulle kunna hoppa in utan några problem.
- Skapa upp sidan först innan ni implementerar API:et. Kolla dock först hur data-strukturen ser ut som kommer från API:et.
- Ställ frågor, men försök först att hitta lösningar själv!
